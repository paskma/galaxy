
var fs = require('fs'),
    PNG = require('node-png').PNG,
    assert = require('assert');

var Picture = exports.Picture = function(width, height, drawCross) {
    this.png = new PNG({
        width: width,
        height: height,
        filterType: -1
    });
    
    this.reset(drawCross);
};

Picture.prototype.reset = function(drawCross) {
    for (var y = 0; y < this.png.height; y++) {
        for (var x = 0; x < this.png.width; x++) {
            var clr;
            if (drawCross) {
                clr = x < (this.png.width >> 1) ^ y < (this.png.height >> 1) ? 0x0 : 0x10;
            } else {
                clr = 0;
            }
            this.set(x, y, [clr, clr, clr, 0xFF]); 
        }
    }
};

Picture.prototype.set = function(x, y, color) {
    assert(x === Math.floor(x));
    assert(y === Math.floor(y));
    var idx = (this.png.width * y + x) << 2;
    this.png.data[idx]     = color[0];
    this.png.data[idx + 1] = color[1];
    this.png.data[idx + 2] = color[2];
    this.png.data[idx + 3] = color[3];
};

Picture.prototype.write = function(filename) {
    this.png.pack().pipe(fs.createWriteStream(filename));
};


