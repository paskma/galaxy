# Galaxy #

Takes a database of stars and creates a WebGL visualization. 

### What is this repository for? ###

* Front-end is a static web.
* Virtually no back-end, only data preprocessing using node.js

### Hints ###

```
npm install
./test.sh
cd data
./download_data.sh
cd ..
./run.sh
./copy.sh
```
