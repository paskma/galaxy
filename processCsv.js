"use strict";


var Stats = require('./stats.js').Stats;
var Picture = require('./picture.js').Picture;
var translate = require('./intervalTranslate.js').translate;
var fs = require('fs');

var process = exports.process = function (reader, resultCb) {

    const INF_DISTANCE = 99000;

    const properties = ['x', 'y', 'z', 'dist', 'proper', 'mag', 'absmag']
    var stats = {
        'x': new Stats(),
        'y': new Stats(),
        'z': new Stats(),
        'dist': new Stats(),
        'mag': new Stats(),
        'absmag': new Stats()
    };

    var propToIndice = new Map();
    var indices;
    var first = true;
    var starList = [];

    reader.addListener('data', data => {    
        if (first) {
            indices = properties.map(item => {return data.indexOf(item);});
            properties.forEach((item, i) => { propToIndice[item] = indices[i]; })
            first = false;
        } else {
            if (data[propToIndice["dist"]] > INF_DISTANCE)
                return;
            
            ['x', 'y', 'z', 'dist', 'mag', 'absmag'].forEach(prop => {
                stats[prop].insert(parseFloat(data[propToIndice[prop]])); 
            });
            
            starList.push({
                "x": data[propToIndice['x']], 
                "y": data[propToIndice['y']], 
                "z": data[propToIndice['z']],
                "dist": data[propToIndice['dist']],
                "name": data[propToIndice['proper']],
                "mag": data[propToIndice['mag']],
                "absmag": data[propToIndice['absmag']]
            });
        }
    }).addListener('end', () => {
        resultCb(starList, stats);
    });
};


var createPicture = exports.createPicture = function(directory, starList, stats) {
    console.log(stats);
        
    const pictureSize = 4000;
    var pictureXY = new Picture(pictureSize, pictureSize);
    var pictureXZ = new Picture(pictureSize, pictureSize);
    var pictureYZ = new Picture(pictureSize, pictureSize);
    const white = [0xff, 0xff, 0xff, 0xff];

    starList.forEach(item => {
        let picCoords = ['x', 'y', 'z'].map(i => {
            return Math.floor(translate([stats[i].min, stats[i].max], [0, pictureSize], item[i]));
        });
        pictureXY.set(picCoords[0], picCoords[1], white);
        pictureXZ.set(picCoords[0], picCoords[2], white);
        pictureYZ.set(picCoords[1], picCoords[2], white);
    });

    pictureXY.write(directory + "/galaxyXY.png");
    pictureXZ.write(directory + "/galaxyXZ.png");
    pictureYZ.write(directory + "/galaxyYZ.png");
};

var writeJson = exports.writeJson = function(filename, starList, stats) {
    console.log(stats);    
    console.info("Writing", starList.length, "records.");
    var data = JSON.stringify(
        starList
        , null, 1);
    fs.writeFile(filename, data);        
};


