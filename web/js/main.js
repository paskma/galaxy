var container, stats;
var camera, controls, scene, renderer;
var objects = [];

var mouse = new THREE.Vector2();
var offset = new THREE.Vector3(10, 10, 10);

init();
animate();

function init() {

    container = document.getElementById("container");

    camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 1, 100000);
    camera.position.z = 100;

    controls = new THREE.TrackballControls(camera);
    controls.rotateSpeed = 10.0;
    controls.zoomSpeed = 2.0;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    scene = new THREE.Scene();

    scene.add(new THREE.AmbientLight(0xffffff));

    var geometry = new THREE.Geometry(),
        defaultMaterial = new THREE.MeshLambertMaterial({
            color: 0xffffff,
            shading: THREE.FlatShading,
            vertexColors: THREE.VertexColors
        });

    function applyVertexColors(g, c) {
        g.faces.forEach(function(f) {
            var n = (f instanceof THREE.Face3) ? 3 : 4;
            for (var j = 0; j < n; j++) {
                f.vertexColors[j] = c;
            }
        });

    }

    var geom = new THREE.BoxGeometry(1, 1, 1);
    var color = new THREE.Color();

    var matrix = new THREE.Matrix4();
    var quaternion = new THREE.Quaternion();

    var addObject = function(positionVector, scaleFactor, color) {
        var position = positionVector;        
        var rotation = new THREE.Euler();
        var scale = new THREE.Vector3();
        scale.x = scaleFactor;
        scale.y = scale.x;
        scale.z = scale.x;

        quaternion.setFromEuler(rotation, false);
        matrix.compose(position, quaternion, scale);

        applyVertexColors(geom, color);

        geometry.merge(geom, matrix);
    };    

    function brightestFirst() {
        STARLIST.sort(function (a, b) {
            return parseFloat(a.absmag) - parseFloat(b.absmag);
        });
    }
    
    function earthVisibleFirst() {
        STARLIST.sort(function (a, b) {
            return parseFloat(a.mag) - parseFloat(b.mag);
        });
    }
    
    function closestFirst() {
        STARLIST.sort(function (a, b) {
            return parseFloat(a.dist) - parseFloat(b.dist);        
        });
    }

    //closestFirst();
    brightestFirst();
    //earthVisibleFirst();

    var count = 0;
    var limit = 70000;
    for (var i = 0; i < STARLIST.length; i++) {
        var star = STARLIST[i];             
        var position = new THREE.Vector3(parseFloat(star.x), parseFloat(star.y), parseFloat(star.z));
        var dist = position.length();

        count++;
        if (count > limit && !star.name)
            continue;
                
        var color = getStarColor(star);
        
        addObject(position, 0.2, color);
    }

    var drawnObject = new THREE.Mesh(geometry, defaultMaterial);
    scene.add(drawnObject);

    renderer = new THREE.WebGLRenderer({
        antialias: true
    });
    renderer.setClearColor(0x0);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.sortObjects = false;
    container.appendChild(renderer.domElement);

    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    container.appendChild(stats.domElement);

    renderer.domElement.addEventListener('mousemove', onMouseMove);

}



function getStarColor(star) {
    var bigDipperStars = ["Dubhe", "Merak", "Phecda", "Megrez", "Alioth", "Mizar", "Alkaid"];
    
    var color = new THREE.Color(0xffffff);    
    if (star.name) {
        if (star.name === "Sol")
            color = new THREE.Color(0x4040ff);
        else if (star.name === "Sirius")
            color = new THREE.Color(0x40ff40);
        else if (bigDipperStars.indexOf(star.name) > -1)
            color = new THREE.Color(0xff0000);
        else
            color = new THREE.Color(0xffff00);
        
    }
    
    return color;
}


//

function onMouseMove(e) {

    mouse.x = e.clientX;
    mouse.y = e.clientY;

}

function animate() {

    requestAnimationFrame(animate);

    render();
    stats.update();

}

function render() {

    controls.update();
    renderer.render(scene, camera);

}
