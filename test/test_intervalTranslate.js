var assert = require('assert');
var translate = require('../intervalTranslate.js').translate;


describe("intervalTranslate", function() {
    it("should keep the value at the same position, relatively", function() {
        assert.equal(1, translate([0, 2], [0, 2], 1));
        assert.equal(2, translate([1, 3], [1, 3], 2));
        assert.equal(2, translate([0, 2], [0, 4], 1));
        assert.equal(2, translate([-1, 1], [0, 4], 0));
    });
});

