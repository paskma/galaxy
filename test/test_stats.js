var assert = require('assert');
var Stats = require('../stats.js').Stats;


describe('Stats', () => {
  stats = new Stats();
  stats.insert(-1);
  stats.insert(2);
  stats.insert(4);
  describe('.min', () => {
    it('should contain minimal value', function () {
      assert.equal(-1, stats.min);
    });
  });
  describe('.max', () => {
    it('should contain maximal value', function () {
      assert.equal(4, stats.max);
    });
  });
  describe('.absMin', () => {
    it('should contain minimal absolute value', function () {
      assert.equal(1, stats.absMin);
    });
  });  
  describe('.count', () => {
    it('should contain number of inserted values', function () {
      assert.equal(3, stats.count);
    });
  });
});

