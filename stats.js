

var Stats = exports.Stats = function() {
    this.min = Number.MAX_VALUE;
    this.max = Number.MIN_VALUE;
    this.absMin = Number.MAX_VALUE;
    this.count = 0;
};

Stats.prototype.insert = function(value) {
    this.min = Math.min(this.min, value);
    this.max = Math.max(this.max, value);
    this.absMin = Math.min(this.absMin, Math.abs(value) );
    this.count++;
};
