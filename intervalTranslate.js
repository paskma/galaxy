

var assert = require('assert');

exports.translate = translate;

function intervalLen(interval) {
    return interval[1] - interval[0];
}

function translate(fromInterval, toInterval, value) {
    assert(fromInterval[0] <= value);
    assert(value <= fromInterval[1]);
    
    return intervalLen(toInterval) * (value - fromInterval[0]) / intervalLen(fromInterval) + toInterval[0];
}


