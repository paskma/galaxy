
var program = require('commander');

"use strict";

var csv = require('ya-csv');
var processCsv = require('./processCsv.js');
var _ = require('lodash');

// equivalent of csv.createCsvFileReader('data.csv') 
var reader = csv.createCsvFileReader('data/HYG-Database/hygdata_v3.csv', {
    'separator': ',',
    'quote': '"',
    'escape': '"',       
    'comment': '',
});

program
  .version('0.0.1')
  .option('-p, --picture', 'Generate picture')
  .option('-j, --json', 'Generate JSON')
  .parse(process.argv);


if (program.picture) {
    var createPictureInDirectory = _.curry(processCsv.createPicture)("out")
    processCsv.process(reader, createPictureInDirectory);
} else if (program.json) {
    var writeJsonToFile = _.curry(processCsv.writeJson)('out/starList.json');
    processCsv.process(reader, writeJsonToFile);
} else {
    program.help();    
}

